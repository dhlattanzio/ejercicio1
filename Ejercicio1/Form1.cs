﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio1
{
    public partial class Form1 : Form
    {
        private int counter;

        public Form1()
        {
            InitializeComponent();
        }

        private void LimpiarCamposProducto()
        {
            textBoxCodigoDeBarras.Text="";
            textBoxNombre.Text="";
            textBoxDescripcion.Text="";
            textBoxPrecio.Text="";
        }

        private void ActualizarSubTotalVenta()
        {
            var venta = (Venta)listBox1.SelectedItem;
            if (venta != null)
            {
                var total = 0.0f;
                foreach (var producto in venta.Productos)
                {
                    total += producto.precio;
                }
                labelSubtotalVenta.Text = "$" + total;
                ActualizarRecaudacionTotal();
            }
        }

        private void ActualizarRecaudacionTotal()
        {
            var total = 0.0f;
            foreach (Venta venta in listBox1.Items)
            {
                foreach (Producto producto in venta.Productos) {
                    total += producto.precio;
                }
            }
            labelRecaudacionTotal.Text = "$" + total;
        }

        private void ActualizarDetallesProducto(Producto producto)
        {
            if (producto != null)
            {
                labelDetallesCodigo.Text = producto.codigoDeBarras;
                labelDetallesNombre.Text = producto.nombre;
                labelDetallesDescripcion.Text = producto.descripcion;
                labelDetallesPrecio.Text = "$" + producto.precio;
            } else
            {
                labelDetallesCodigo.Text = "-";
                labelDetallesNombre.Text = "-";
                labelDetallesDescripcion.Text = "-";
                labelDetallesPrecio.Text = "-";
            }
        }

        // Crea una nueva venta
        private void button2_Click(object sender, EventArgs e)
        {
            var venta = new Venta(++counter);
            listBox1.Items.Add(venta);
        }

        // Actualiza la lista de productos al cambiar de venta
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var venta = (Venta)listBox1.SelectedItem;
            if (venta != null)
            {
                listBox2.Items.Clear();
                foreach (Producto p in venta.Productos)
                {
                    listBox2.Items.Add(p);
                }
                ActualizarSubTotalVenta();
                ActualizarDetallesProducto(null);
            }
        }

        // Agregar producto a una venta
        private void button1_Click(object sender, EventArgs e)
        {
            var venta = (Venta)listBox1.SelectedItem;
            if (venta != null)
            {
                var codigoDeBarras = textBoxCodigoDeBarras.Text;
                var nombre = textBoxNombre.Text;
                var descripcion = textBoxDescripcion.Text;
                var precioTexto = textBoxPrecio.Text;
                if (string.IsNullOrWhiteSpace(codigoDeBarras) ||
                    string.IsNullOrWhiteSpace(nombre) ||
                    string.IsNullOrWhiteSpace(descripcion) ||
                    string.IsNullOrWhiteSpace(precioTexto))
                {
                    MessageBox.Show("No puede haber campos vacios");
                } else
                {
                    if (float.TryParse(precioTexto, out var precio))
                    {
                        var producto = new Producto(codigoDeBarras, nombre, descripcion, precio);
                        listBox2.Items.Add(producto);
                        venta.AgregarProducto(producto);
                        LimpiarCamposProducto();
                        ActualizarSubTotalVenta();
                    }
                    else
                    {
                        MessageBox.Show("El formato del precio es invalido");
                    }
                }
            } else
            {
                MessageBox.Show("Selecciona una venta");
            }
            
        }

        // Eliminar producto de una venta
        private void button3_Click(object sender, EventArgs e)
        {
            var producto = (Producto)listBox2.SelectedItem;
            if (producto != null)
            {
                var venta = (Venta)listBox1.SelectedItem;
                listBox2.Items.Remove(producto);
                venta.EliminarProducto(producto);
                ActualizarSubTotalVenta();
                ActualizarDetallesProducto(null);
            } else
            {
                MessageBox.Show("Selecciona un producto");
            }
        }

        // Actualizar detalles al cambiar de Producto
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var producto = (Producto)listBox2.SelectedItem;
            ActualizarDetallesProducto(producto);
        }

        // Eliminar una venta
        private void button4_Click(object sender, EventArgs e)
        {
            var venta = (Venta)listBox1.SelectedItem;
            if (venta != null)
            {
                listBox1.Items.Remove(venta);
                ActualizarDetallesProducto(null);
                ActualizarRecaudacionTotal();

                listBox2.Items.Clear();
                labelSubtotalVenta.Text = "-";
            } else
            {
                MessageBox.Show("Selecciona una venta");
            }
        }
    }
}
